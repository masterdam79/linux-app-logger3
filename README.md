# Active app/window logger for linux

Just a tiny bash script for quantified self purposes.

## INSTALL

1. Clone repo.
2. Install wmctrl
3. Adjust paths in logger.sh script.
4. Add it to Startup Applications (for Ubuntu)

By default you'll get logged active app name and active window title every time the user switched active window. It's up to you what to do with that data ;)
