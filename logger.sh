#!/usr/bin/env bash

currentDateShort=$(date "+%Y%m%d")

if [ ! -d $HOME/LOG ]
then
	mkdir $HOME/LOG
fi

process_name="linux-app-logger3/logger.sh"

# Current process ID
current_pid=$$

# Check if a previous instance is running
if pids=$(pgrep -f "$process_name" | grep -vw "$current_pid"); then
    echo "Previous instance(s) of $process_name is running with PID(s): $pids. Killing them..."
    kill $pids
    sleep 2 # Optional: Allow time for the processes to terminate gracefully
fi

while true;
do
	currentDateLong=$(date "+%FT%T%Z")
	LASTLINE=$(tail -1 $HOME/LOG/${currentDateShort}-$(hostname -s).log | awk '{ print substr($0, index($0,$2)) }')
	WINID=$(xprop -root | grep '_NET_ACTIVE_WINDOW(WINDOW)' | awk '{ print $5 }' | awk -F'x' '{ print $2 }')
	WINTITLE=$(wmctrl -lx | grep ${WINID} | awk '{ print substr($0, index($0,$3)) }')
	NEWLOGLINE="${currentDateLong}\t${WINTITLE}"
	LASTMD5=$(echo ${LASTLINE} | md5sum)
	NEWMD5=$(echo ${WINTITLE} | md5sum)
	if [[ "${WINID}" != "0" ]]; then
		if [[ "${LASTMD5}" != "${NEWMD5}" ]];
		then
			echo -e "${NEWLOGLINE}" >> $HOME/LOG/${currentDateShort}-$(hostname -s).log
		fi
	fi
	sleep 1
done
